﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PanelHead : MonoBehaviour
{
   /* public Image btnHead;
    public Color pageOn = Color.red;
    public Color pageOff = Color.green;*/

    public GameObject PanelHeadpn;
    public float TranformTime = 1f;
    public Vector3 TargetScale = new Vector3(1f, 1f, 1f);
    /*public bool sttPanel;*/
            
   
    void Start()
    {
      
    }

    public void PageOn()
    {
        PanelHeadpn.transform.DOMoveX(0, TranformTime);
        PanelHeadpn.transform.DOScale(TargetScale, TranformTime);
    }
    public void PageOff()
    {
        PanelHeadpn.transform.DOMoveX(50, TranformTime);
        PanelHeadpn.transform.DOScale(Vector3.zero, TranformTime);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
