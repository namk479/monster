﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using TMPro;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController intance;
    [SerializeField] private SpriteRenderer objHead;
    [SerializeField] private SpriteRenderer objEye;
    [SerializeField] private SpriteRenderer objMouth;
    [SerializeField] private SpriteRenderer objAcc;
    [SerializeField] private SpriteRenderer objBody;
    [SerializeField] private Sprite[] _sprHead;
    [SerializeField] private Sprite[] _sprEye;
    [SerializeField] private Sprite[] _sprMouth;
    [SerializeField] private Sprite[] _sprAcc;
    [SerializeField] private Sprite[] _sprBody;
    [SerializeField] private int coin;
    public TextMeshProUGUI coinText;
    public GameObject Body;
    public GameObject Face;
    public GameObject Player;
    [SerializeField] public bool sttPage;
    public GameObject btnnext;
    public GameObject btnDone;
    public BUTTON_MODE modeName;
  

    // Start is called before the first frame update
    private void Awake()
    {
        intance = this;
      
    }
  /* public void Start()
    {
        objHead.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
         objEye.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        objHead.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        objHead.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        //Khi bắt đầu, kiểm tra xem có giá trị Coin đã lưu trong PlayerPrefs hay không
    }*/
    public void SelectHead(int id)
    {
        Debug.Log(id);
        objHead.sprite = _sprHead[id];
        btnnext.gameObject.SetActive(true);
        modeName = BUTTON_MODE.Head;
        drag.instance.SetUp(BUTTON_MODE.Head.ToString());
    }
    public void SelectEye(int id)
    {
        objEye.sprite = _sprEye[id];
        btnnext.gameObject.SetActive(true);
        modeName = BUTTON_MODE.Eye;
        drag.instance.SetUp(BUTTON_MODE.Eye.ToString());
    }
    public void SelectMouth(int id)
    {
        objMouth.sprite = _sprMouth[id];
        btnnext.gameObject.SetActive(true);
        modeName = BUTTON_MODE.Mouth;
        drag.instance.SetUp(BUTTON_MODE.Mouth.ToString());
    }
    public void SelectAcc(int id)
    {
        objAcc.sprite = _sprAcc[id];
        btnnext.gameObject.SetActive(true);
        modeName = BUTTON_MODE.Acc;
        drag.instance.SetUp(BUTTON_MODE.Acc.ToString());
    }
    public void SelectBody(int id)
    {
        objBody.sprite = _sprBody[id];
        //btnnext.gameObject.SetActive(true);
        btnDone.gameObject.SetActive(true);
        modeName = BUTTON_MODE.Body;
        drag.instance.SetUp(BUTTON_MODE.Body.ToString());
    }
}
public enum BUTTON_MODE
{
    Head = 0,
    Eye = 1,
    Mouth = 2,
    Acc = 3,
    Body =4,

}
