﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoadImage : MonoBehaviour
{

    public Image imageComponent;
    public string imagePath = "Images/myImage";

    private void Start()
    {
        // Tải hình ảnh từ thư mục Resources
        Sprite sprite = Resources.Load<Sprite>(imagePath);

        // Thiết lập hình ảnh cho component Image
        imageComponent.sprite = sprite;
    }
}
