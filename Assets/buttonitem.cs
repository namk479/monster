﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class buttonitem : MonoBehaviour
{
    public BUTTON_MODE Mode;
    public int idMe;
    private GameController _gameController;
    public TextMeshProUGUI textCoinButoon;
    public Button Buy;
    public GameObject UIBUyitem;
   // public Button yes;
    public TextMeshProUGUI BuyDone;
    public bool _unlock;
    [SerializeField] private Image _ImgMe;
    [SerializeField] private GameObject _Select;
    public static buttonitem insBtnIem;
    private string unlockKey; // Biến lưu khóa cho trạng thái mở khóa của loại mode
    public Image bg;
    public List<Sprite> spritesbG = new List<Sprite>();
    public Sprite sprtBg;


    public void Awake()
    {
        //insBtnItem = this;
    }
    public void OnEnable()
    {
        SetRandomBackground();
    }
    public void SetRandomBackground()
    {
        bg.sprite = sprtBg;
       /* if (spritesbG.Count > 0 && bg != null)
        {
            int randomIndex = Random.Range(0, spritesbG.Count);
            Sprite selectedSprite = spritesbG[randomIndex];
            bg.sprite = selectedSprite;
        }
        else
        {
            Debug.LogError("Background sprites list is empty or SpriteRenderer is not assigned.");
        }*/
    }
    public void SetData(BUTTON_MODE modeData, int idData, Sprite sprData, bool unlock)
    {
        Mode = modeData;
        idMe = idData;
        _gameController = GameController.intance;
        _ImgMe.sprite = sprData;
        _ImgMe.SetNativeSize();
    }
 
    public void ClickButton()
    {
        //if (_unlock == true)
     //   {
            switch (Mode)
            {
                case BUTTON_MODE.Head:
                    _gameController.SelectHead(idMe);
                    break;
                case BUTTON_MODE.Eye:
                    _gameController.SelectEye(idMe);
                    break;
                case BUTTON_MODE.Mouth:
                    _gameController.SelectMouth(idMe);
                    break;
                case BUTTON_MODE.Acc:
                    _gameController.SelectAcc(idMe);
                    break;
                case BUTTON_MODE.Body:
                    _gameController.SelectBody(idMe);
                    break;
            }
        //  }
    }

   /* public void UnlockButton()
    {
        _unlock = true;
        Buy.interactable = false;
        BuyDone.text = "Da Mua";
        UpdateUI();
    }

    private void UpdateUI()
    {
        if (_unlock)
        {
            Buy.interactable = false;
            BuyDone.text = "Da Mua";
        }
    }*/

}
