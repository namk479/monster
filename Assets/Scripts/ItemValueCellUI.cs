using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ItemValueCellUI : MonoBehaviour
{
    public ItemID ItemID;
    public TextMeshProUGUI txtValue;
    void Refresh()
    {
        txtValue.text = User.Instance[ItemID].ToString();
    }
}
