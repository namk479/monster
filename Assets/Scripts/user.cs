using JetBrains.Annotations;
using System.Collections.Generic;
//using Sirenix.OdinInspector;

using System.Linq;
using System.Runtime.Serialization;
using UnityEditor;
using UnityEngine;
using static UnityEditor.FilePathAttribute;
//using static  

[CreateAssetMenu(fileName = "User", menuName = "Nam/User", order = 0)]
public class User : ScriptableObject
{
    private static User instance;
    public static User Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<User>("Manager/User");
            }

            return instance;
        }
    }

    public List<ItemValue> defaultItems;
    private UserData userData;
    private const string UserDataPath = "UserDataPath";

    public void Init()
    {
        userData = DataPersistent.ReadDataExist<UserData>(UserDataPath);
        if (userData == null)
        {
            userData = new UserData();
            userData.Init();
            Save();
        }

        CheckDefaultItem();
    }

    void CheckDefaultItem()
    {
        bool needToSave = false;

        foreach (ItemValue defaultItem in defaultItems)
        {
            if (!userData.items.ContainsKey(defaultItem.item))
            {
                userData.items.Add(defaultItem.item, defaultItem.value);
                needToSave = false;
            }
        }

        if (needToSave)
        {
            Save();
        }
    }

    public void Save()
    {
        DataPersistent.SaveData<UserData>(UserDataPath, userData);
    }

    public void Clear()
    {
        DataPersistent.ClearData(UserDataPath);
    }

    public int this[ItemID id]
    {
        get
        {
            if (!userData.items.ContainsKey(id))
            {
                return 0;
            }
            return userData.items[id];
        }
        set
        {
            if (!userData.items.ContainsKey(id))
            {
                userData.items.Add(id, value);
                Earn(id, value);
            }
            else
            {
                int change = value - userData.items[id];
                if (change > 0)
                {
                    Earn(id, change);
                }
                else
                {
                    Spend(id, -change);
                }

                userData.items[id] = value;
            }

            Save();
        }
    }

    void Earn(ItemID id, int value)
    {
        if (id != ItemID.Gold)
        {
            return;
        }
    }

    void Spend(ItemID id, int value)
    {
        if (id != ItemID.Gold)
        {
            return;
        }
    }

    /*
        public List<ItemID> lstGunclaim()
        {
            return userData.gunclaim;
        }*/
}
[System.Serializable]
public class UserData
{
    public Dictionary<ItemID, int> items;
    public void Init()
    {
        User.Instance[ItemID.Gold] += 1000;
    }
}
[System.Serializable]
public class ItemValue
{
    public ItemID item;
    public int value;

    public ItemValue(ItemID _item, int _value)
    {
        item = _item;
        value = _value;

    }

    public ItemValue Clone()
    {
        return new ItemValue(item, value);
    }
}
