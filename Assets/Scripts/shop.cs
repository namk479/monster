﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class shop : MonoBehaviour
{
    [System.Serializable]
    public class ItemTech
    {
        public int iditem;
        public Sprite sprite;
        public int coin;
        public bool unlock;
    }

    public GameObject buyitem;
    public buttonitem buttonPrefab; // Renamed 'buttonitem' to 'ButtonItem' for consistency.
    public BUTTON_MODE ModeScroll;
    public Transform content;
    public List<ItemTech> items = new List<ItemTech>();
    //public List<buttonitem> itemsBtnList = new List<buttonitem>();
    public static shop instance; // Renamed 'insShopInstance' to 'instance' for consistency.

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        instListItems();
    }
    public void instListItems()
    {
        for (int i = 0; i < items.Count; i++)
        {
            int itemIndex = i;
            buttonitem buttonItem = Instantiate(buttonPrefab, content) as buttonitem;
            buttonItem.SetData(ModeScroll, i, items[i].sprite, buttonItem._unlock);
            /*  buttonItem._unlock = items[i].unlock = true;*/
            TextMeshProUGUI coinText = buttonItem.textCoinButoon;
            coinText.text = items[i].coin.ToString();
            Button btnBuy = buttonItem.Buy;
            btnBuy.interactable = !buttonItem._unlock;
        }
    }
    // Method to refresh the UI after moving an item to the top
/*    private void RefreshUI()
    {
        // Remove old buttons
        foreach (var buttonItem in itemsBtnList)
        {
            Destroy(buttonItem.gameObject);
        }
        itemsBtnList.Clear();
    }*/
}
