﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
public class UiManagertest : MonoBehaviour
{
    public GameObject[] gameObjectsPanel;
    // public GameObject PanelStart;
    // public GameObject PanelGamePlay;
    public Image[] btnItem;
    public GameObject[] SttPageOn;
    public GameObject[] SttPageOFF;
    public Button NextButton;
    public Button PrevButton;
    public Button HomeButton;
    public Button StartBtton;
    private int currentIndex = 0;
    public GameObject[] BtnON;
    public Gamemanager Gammanage;
    public GameState Gamestete;
    public static UiManagertest instance;
    public Button DoneButton;
    public Button[] btnAvt;
    [SerializeField] private GameObject playerObj;
    public Button btnPlay;
    public Button Oncemore;

    public static Gamemanager instanec;
    public UiManagertest ui;
    public GameObject Face;
    public GameObject Bodyl;
    public GameObject PanelHome;
    public GameObject PaneGame;
    public GameObject PaneEndeGame;
    public GameObject ResetSenec;
    public GameObject btnOnemore;

    public void Awake()
    {
        instance = this;
    }
    private void OnEnable()
    {
        for (int i = 1; i < gameObjectsPanel.Length; i++)
        {
            gameObjectsPanel[i].SetActive(false);

        }
        for (int i = 1; i < btnItem.Length; i++)
        {
            BtnON[i].gameObject.SetActive(false);
        }
        DoneButton.onClick.RemoveListener(ClickDone);
        DoneButton.onClick.AddListener(ClickDone);


        Oncemore.onClick.RemoveListener(ResetScene);
        Oncemore.onClick.AddListener(() =>
        {
            ResetScene();
        });


        btnPlay.onClick.RemoveListener(() =>
        {

           PanelgameSta();
        });
        btnPlay.onClick.AddListener(() =>
        {

            PanelgameSta();
        });

        HomeButton.onClick.RemoveListener(() =>
        {

            HomeState();
        });
        HomeButton.onClick.AddListener(() =>
        {
            HomeState();
        });
        /*Oncemore.onClick.AddListener(()=>{ResetScene().OnComplete(click) });*/
    }
    private void Start()
    {


    }

    public void Onclickbtn(int val)
    {
        //if(val <= currentIndex) { }
        BtnON[val].SetActive(true);
        currentIndex = val;
        for (int i = 0; i < BtnON.Length; i++)
        {
            if (i != val)
            {
                BtnON[i].SetActive(false);
                //BtnON[i].gameObject.GetComponent<Button>().interactable = true;
            }
            else if (i == val)
            {
                BtnON[i].SetActive(true);
            }
        }

        for (int j = 0; j < btnAvt.Length; j++)
        {
            if (j != currentIndex)
            {
                btnAvt[j].interactable = true;
            }
            else if (j == currentIndex)
            {
                btnAvt[j].interactable = false;
            }
        }
    }
    public void OnClickBtnPrev()
    {
        PrevButton.onClick.RemoveAllListeners();
        PrevButton.onClick.AddListener(() =>
        {
            gameObjectsPanel[currentIndex].SetActive(false);
            BtnON[currentIndex].gameObject.SetActive(true);
            currentIndex--;
            if (currentIndex < 0)
            {
                currentIndex = gameObjectsPanel.Length - 1;
            }
            gameObjectsPanel[currentIndex].SetActive(true);
            BtnON[currentIndex].gameObject.SetActive(false);
        });
    }

    public void OnClickBtnNext()
    {
        NextButton.onClick.RemoveAllListeners();
        NextButton.onClick.AddListener(() =>
        {
            gameObjectsPanel[currentIndex].SetActive(false);
            BtnON[currentIndex].gameObject.SetActive(false);
            currentIndex = (currentIndex + 1) % gameObjectsPanel.Length;
            gameObjectsPanel[currentIndex].SetActive(true);
            BtnON[currentIndex].gameObject.SetActive(true);
            NextButton.gameObject.SetActive(false);
            Debug.Log(currentIndex);
            if (currentIndex == 0)
            {
                //Debug.Log("");
                Gamestete = GameState.ChangeFace;
            }
            else if (currentIndex == 4)
            {
                Gamestete = GameState.ChangBOdy;
            }
            if (ui.Gamestete == GameState.ChangBOdy)
            {
                // Face.gameObject.transform.localScale = new Vector3(0.4f,0.4f,0.4f);
                Face.gameObject.transform.DOScale(new Vector3(0.4f, 0.4f, 0.4f), 0.1f);
                Face.gameObject.transform.DOMove(new Vector3(-0.05f, 1.5f, 0f), 1f);
                Bodyl.gameObject.SetActive(true);
            }
            if (ui.Gamestete == GameState.ChangeFace)
            {
                //Face.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                Face.gameObject.transform.DOScale(new Vector3(1f, 1f, 1f), 1f);
                Bodyl.gameObject.SetActive(false);
            }
        });
    }
    public void ClickDone()
    {
        Gamestete = GameState.RunDance;
        PanelEnd();
        playerObj.transform.DOMove(new Vector3(0f, 0f, 0f), 1f);
        playerObj.transform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 1f);
    }
    public void ResetScene()
    {
        // Lấy tên của scene hiện tại
        string currentSceneName = SceneManager.GetActiveScene().name;

        // Load lại scene hiện tại
        SceneManager.LoadScene(currentSceneName);
    }



 /*   public void Awake()
    {
        instanec = this;
    }
    public void OnEnable()
    {
        AudioManager.instance.Play("wingame");

    }*/
    public void Update()
    {
        
    }

    public void HomeState()
    {
        PanelHome.SetActive(true);
        PaneGame.SetActive(false);
        PaneEndeGame.SetActive(false);
        GameController.intance.Player.gameObject.SetActive(false);


    }
    public void PanelgameSta()
    {
        PanelHome.SetActive(false);
        PaneGame.SetActive(true);
        PaneEndeGame.SetActive(false);
        GameController.intance.Player.gameObject.SetActive(true);

    }
    public void PanelEnd()
    {
        PanelHome.SetActive(false);
        PaneGame.SetActive(false);
        PaneEndeGame.SetActive(true);
    }
    public void PauseGame()
    {

    }
    public void GameWin()
    {

    }
    public void SelectFace()
    {

    }
}

public enum GameState
{
    ChangeFace,
    ChangBOdy,
    RunDance,
}
