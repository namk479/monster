using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ShopInventory", menuName = "ShopData/Shop", order = 1)]
[System.Serializable]

public class ShopInventorySctipstable : ScriptableObject
{
    public bool isUnlock;
    public NameSkin name;
    public ItemValue itemcell;
    public Sprite Icon;

    public Sprite imgItem;
}

public enum NameSkin
{
    Acc1, Acc2, Acc3, Acc4, Acc5, Acc6, Acc7,

}
