using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : MonoBehaviour
{
    public static int IdHead
    {
        get 
        {
            return PlayerPrefs.GetInt("idhead", 0);
        }
        set
        {
            PlayerPrefs.SetInt("idhead", value);
        }
    }
        
}
