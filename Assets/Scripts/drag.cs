using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class drag : MonoBehaviour
{
    private Vector3 offset;
    private bool isDragging = false;
    //public BUTTON_MODE NAMEMODE;
    public string idname;
    public static drag instance;
    public void Awake()
    {
        instance = this;
    }
    public void SetUp(string idname)
    {
        if (idname == GameController.intance.modeName.ToString())
        {
            isDragging = true;
        }
        else if(idname != GameController.intance.modeName.ToString())
        {
            isDragging = false;
        }
    }


    private void OnMouseDown()
    {
        if (isDragging == true)
        {
        offset = gameObject.transform.position - GetMouseWorldPos();
        }

    }

    private void OnMouseDrag()
    {
        if (isDragging)
        {
            transform.position = GetMouseWorldPos() + offset;
        }
    }

    private void OnMouseUp()
    {
        isDragging = false;
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = Camera.main.nearClipPlane;
        return Camera.main.ScreenToWorldPoint(mousePos);
    }
}
