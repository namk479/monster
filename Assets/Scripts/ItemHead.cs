using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemHead : MonoBehaviour
{
     public BUTTON_MODE Mode;
    public int idMe;
    private GameController _gameController;
    public TextMeshProUGUI textCoinButoon;
    public Button Buy;
    public TextMeshProUGUI BuyDone;
    public bool _unlock;

    [SerializeField] private Image _ImgMe;
    [SerializeField] private GameObject _Select;
    public static ItemHead insBtnItemHead;

    public void Awake()
    {
        insBtnItemHead = this;
    }

    public void SetData(BUTTON_MODE modeData, int idData, Sprite sprData, bool unlock)
    {
        Mode = modeData;
        idMe = idData;
        _gameController = GameController.intance;
        _ImgMe.sprite = sprData;
        _ImgMe.SetNativeSize();
        _unlock = unlock;
        UpdateUI();
    }

    public void ClickButton()
    {
        if (_unlock == true)
        {
            switch (Mode)
            {
                case BUTTON_MODE.Head:
                    _gameController.SelectHead(idMe);
                    break;
                case BUTTON_MODE.Eye:
                    _gameController.SelectEye(idMe);
                    break;
                case BUTTON_MODE.Mouth:
                    _gameController.SelectMouth(idMe);
                    break;
                case BUTTON_MODE.Acc:
                    _gameController.SelectAcc(idMe);
                    break;
                case BUTTON_MODE.Body:
                    _gameController.SelectBody(idMe);
                    break;
            }
        }
    }

    public void UnlockButton()
    {
        _unlock = true;
        Buy.interactable = false;
        BuyDone.text = "Da Mua";
        UpdateUI();
    }

    private void UpdateUI()
    {
        if (_unlock)
        {
            Buy.interactable = false;
            BuyDone.text = "Da Mua";
        }
    }
}
